import React, { Component } from 'react'
import SearchBar  from './components/SearchBar.js'
import UserList  from './components/UserList'

export default class App extends Component {
  constructor(props) {
    super(props);
    var app = this;
    app.state = {
      items: []
    };
    fetch('data.json')
    .then(function(response) {
        return response.json();
    })
    .then(function(items) {
        app.setState({
            items: items
        });
    });
  }

  render() {
    return (
      <div className="container app">
	  	<SearchBar/>
        <UserList items={this.state.items} />
      </div>
    );
  }
}
