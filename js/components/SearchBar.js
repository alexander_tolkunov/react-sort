import React, { Component } from 'react';

const SearchBar = (filter) => (
    <div className="search-bar">
		<input type="search" className="form-control input-lg" placeholder="Search" />
	</div>
);

export default SearchBar;
