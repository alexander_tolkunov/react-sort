import React, { Component } from 'react';

const UserData = ({ user }) => (
    <tr>
        <td>
            <img src={`images/${user.image}.svg`} alt={user.name}/>
        </td>
        <td>{user.name}</td>
        <td>{user.age}</td>
        <td>{user.phone}</td>
    </tr>
);

export default UserData;
