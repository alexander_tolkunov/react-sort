import React, { Component } from 'react'
import UserData  from './UserData'

function getItems(items) {
  return items.map((item, i) => <UserData key={item.name} user={item} />);
}

const UserList = ({ items }) => (
	<table className="table table-hover">
		<thead>
			<tr>
				<th>Photo</th>
				<th>Name</th>
				<th>Age</th>
				<th>Phone</th>
			</tr>
		</thead>
		<tbody>
			{getItems(items)}
		</tbody>
	</table>
);

export default UserList;
